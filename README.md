#php Base 类、单例模式抽象类、全局注册表抽象类

## install 

```
composer require man0sions/base
```

## useage

### 1.1 Instance 单例模式抽象类

```

abstract class Instance
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * Instance constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return mixed
     */
    public static function instance()
    {
        if (!static::$instance) {
            static:: $instance = new static();
        }
        return static::$instance;
    }
}
```

### 1.2 Registry 全局注册表抽象类

```

abstract class Registry extends Instance
{
    protected static $instance; //继承Instance类 必须重写protected static $instance;否则创建对象不正确

    protected $data = [];

    public static function getValue($key)
    {
        return static::instance()->get($key);
    }

    public static function setValue($key, $value)
    {
        return static::instance()->set($key, $value);
    }

    public function get($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }
}
```

### 2.1 SessionRegistry 全局session注册表
```
\LuciferP\Base\SessionRegistry::setValue('name','zhansag');
var_dump(\LuciferP\Base\SessionRegistry::getValue('name'));

or

\LuciferP\Base\SessionRegistry::instance()->set('name','lisi');
\LuciferP\Base\SessionRegistry::instance()->get('name');

```

### 2.2 ApplicationRegistry 全局app(配置文件)注册表

```
\LuciferP\Base\ApplicationRegistry::setConfig('aaa');

var_dump(\LuciferP\Base\ApplicationRegistry::getConfig());

```
