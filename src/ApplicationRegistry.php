<?php
/**
 * Created by PhpStorm.
 * @author Luficer.p <81434146@qq.com>
 * Date: 16/10/28
 * Time: 上午10:52
 */

namespace LuciferP\Base;

/**
 * Class ApplicationRegistry
 * @package LuciferP\Base
 * @author Luficer.p <81434146@qq.com>
 */

class ApplicationRegistry extends Registry
{
    const config_key = 'main_conf';
    protected static $instance;

    public static function getConfig()
    {
        return static::instance()->get(static::config_key);
    }

    public static function setConfig($value)
    {
        static::instance()->set(static::config_key, $value);
    }

}