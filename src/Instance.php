<?php
/**
 * Created by PhpStorm.
 * @author Luficer.p <81434146@qq.com>
 * Date: 16/11/3
 * Time: 上午11:29
 */

namespace LuciferP\base;

/**
 * 单例模式抽象接口
 * Class Instance
 * @package LuciferP\base
 * @author Luficer.p <81434146@qq.com>
 */

abstract class Instance
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * Instance constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return mixed
     */
    public static function instance()
    {
        if (!static::$instance) {
            static:: $instance = new static();
        }
        return static::$instance;
    }
}