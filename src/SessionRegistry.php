<?php
/**
 * Created by PhpStorm.
 * @author Luficer.p <81434146@qq.com>
 * Date: 16/10/28
 * Time: 上午10:52
 */

namespace LuciferP\Base;

/**
 * Class SessionRegistry
 * @package LuciferP\Base
 * @author Luficer.p <81434146@qq.com>
 */
class SessionRegistry extends Registry
{
    protected static $instance;

    protected function __construct()
    {
        session_start();
    }

    protected function get($key)
    {
        if (isset($_SESSION[__CLASS__][$key])) {
            return $_SESSION[__CLASS__][$key];
        }
        return null;
    }

    protected function set($key, $value)
    {
        $_SESSION[__CLASS__][$key] = $value;
    }


}