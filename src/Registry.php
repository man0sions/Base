<?php
/**
 * Created by PhpStorm.
 * @author Luficer.p <81434146@qq.com>
 * Date: 16/10/28
 * Time: 上午10:43
 */

namespace LuciferP\Base;

/**
 * Class Registry
 * @package LuciferP\Base
 * @author Luficer.p <81434146@qq.com>
 */
abstract class Registry extends Instance
{
    protected static $instance;

    protected $data = [];

    public static function getValue($key)
    {
        return static::instance()->get($key);
    }

    public static function setValue($key, $value)
    {
        return static::instance()->set($key, $value);
    }

    public function get($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }
}